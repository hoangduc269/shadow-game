﻿using UnityEngine;
using System;
using System.Collections;

namespace EZ_Pooling
{
    /// <summary>
    /// PrefabPool class (for holding data in the editor inspector)
    /// </summary>
    [Serializable]
    public class EZ_PrefabPoolOption
    {
        public Transform PrefabTransform;
        public int InstancesToPreload = 1;
        public bool IsPoolExpanded = true;
        public bool ShowDebugLog = false;
        public bool PoolCanGrow = false;
        public bool CullDespawned = false;
        public int CullAbove = 10;
        public float CullDelay = 2f;
        public int CullAmount = 1;
        public bool EnableHardLimit = false;
        public int HardLimit = 10;
        public bool recycle = false;
    }

}
