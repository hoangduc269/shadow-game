﻿using UnityEngine;
using UnityEditor;
using System.Collections;


namespace EZ_Pooling
{
    /// <summary>
    /// custom inspector class used to display the pool manager gui functionalities 
    /// </summary>
    [CustomEditor(typeof(EZ_PoolManager))]
    public class EZ_PoolManager_Insp : Editor
    {
        private EZ_PoolManager poolManager;

        private Vector2 scrollPos = Vector2.zero;

        public override void OnInspectorGUI()
        {
            EditorGUI.indentLevel = 0;
            GUI.contentColor = Color.white;
            bool isDirty = false;
            poolManager = (EZ_PoolManager)target;


            poolManager.AutoAddMissingPrefabPool = EditorGUILayout.Toggle("Auto Add Missing Items", poolManager.AutoAddMissingPrefabPool);
            poolManager.ShowDebugLog = EditorGUILayout.Toggle("Show Debug Log", poolManager.ShowDebugLog);
            poolManager.UsePoolManager = EditorGUILayout.Toggle("Use EZ Pool Manager", poolManager.UsePoolManager);

            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

            EditorGUI.indentLevel = 1;

            poolManager.IsRootExpanded = EditorGUILayout.Foldout(poolManager.IsRootExpanded, string.Format("Pools ({0})", poolManager.PrefabPoolOptions.Count));

            // Add expand / collapse buttons if there are items in the list
            if (poolManager.PrefabPoolOptions.Count > 0)
            {
                EZ_EditorUtility.BeginColor(EZ_EditorAssets.ShiftPosColor);
                var masterCollapse = GUILayout.Button("Collapse All", EditorStyles.toolbarButton, GUILayout.Width(80));

                var masterExpand = GUILayout.Button("Expand All", EditorStyles.toolbarButton, GUILayout.Width(80));
                EZ_EditorUtility.EndColor();

                if (masterExpand)
                {
                    foreach (var item in poolManager.PrefabPoolOptions)
                    {
                        item.IsPoolExpanded = true;
                    }
                }

                if (masterCollapse)
                {
                    foreach (var item in poolManager.PrefabPoolOptions)
                    {
                        item.IsPoolExpanded = false;
                    }
                }
            }
            else
            {
                GUILayout.FlexibleSpace();
            }

            //only enable adding of pools when the application is NOT in play state
            if (!Application.isPlaying) //During Editor
            {
                EZ_EditorUtility.BeginColor(EZ_EditorAssets.AddBtnColor);
                if (GUILayout.Button("Add", EditorStyles.toolbarButton, GUILayout.Width(32)))
                {
                    poolManager.PrefabPoolOptions.Insert(0, new EZ_PrefabPoolOption()); //add to the top of the list
                    isDirty = true;
                }
                EZ_EditorUtility.EndColor();
            }

            EditorGUILayout.EndHorizontal();

            if (poolManager.IsRootExpanded)
            {
                int i_ToRemove = -1;
                int i_ToInsertAt = -1;
                int i_ToShiftUp = -1;
                int i_ToShiftDown = -1;

                EditorGUILayout.BeginVertical();

                scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(0), GUILayout.Height(0));

                for (var i = 0; i < poolManager.PrefabPoolOptions.Count; ++i)
                {
                    var prefabPoolOption = poolManager.PrefabPoolOptions[i];

                    EditorGUI.indentLevel = 2;
                    EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
                    var name = prefabPoolOption.PrefabTransform == null ? "[NO PREFAB]" : prefabPoolOption.PrefabTransform.name;
                    prefabPoolOption.IsPoolExpanded = EditorGUILayout.Foldout(prefabPoolOption.IsPoolExpanded, name, EditorStyles.foldout);

                    EZ_EditorUtility.BeginColor(EZ_EditorAssets.ShiftPosColor);

                    if (i > 0)
                    {
                        // the up arrow.
                        if (GUILayout.Button("▲", EditorStyles.toolbarButton, GUILayout.Width(24)))
                        {
                            i_ToShiftUp = i;
                        }
                    }
                    else
                    {
                        GUILayout.Space(24);
                    }

                    if (i < poolManager.PrefabPoolOptions.Count - 1)
                    {
                        // The down arrow will move things towards the end of the List
                        if (GUILayout.Button("▼", EditorStyles.toolbarButton, GUILayout.Width(24)))
                        {
                            i_ToShiftDown = i;
                        }
                    }
                    else
                    {
                        GUILayout.Space(24);
                    }

                    EZ_EditorUtility.EndColor();

                    //only enable adding or deleting of pools when the application is NOT in play state
                    if (!Application.isPlaying) //During Editor
                    {
                        EZ_EditorUtility.BeginColor(EZ_EditorAssets.AddBtnColor);
                        if (GUILayout.Button("Add", EditorStyles.toolbarButton, GUILayout.Width(32)))
                        {
                            i_ToInsertAt = i + 1;
                        }
                        EZ_EditorUtility.EndColor();

                        EZ_EditorUtility.BeginColor(EZ_EditorAssets.DelBtnColor);
                        if (GUILayout.Button("Del", EditorStyles.toolbarButton, GUILayout.Width(32)))
                        {
                            i_ToRemove = i;
                        }
                        EZ_EditorUtility.EndColor();
                    }

                    EditorGUILayout.EndHorizontal();

                    if (prefabPoolOption.IsPoolExpanded)
                    {
                        EditorGUI.indentLevel = 1;


                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space(12); //offset the prefab preview towards the center

                        Texture prefabPreviewIcon = null;
                        if (prefabPoolOption.PrefabTransform && prefabPoolOption.PrefabTransform.GetComponent<Renderer>() != null)
                        {
                            prefabPreviewIcon = AssetPreview.GetAssetPreview(prefabPoolOption.PrefabTransform.gameObject);
                        }
                        else
                        {
                            prefabPreviewIcon = EZ_EditorUtility.LoadTexture(EZ_EditorAssets.MissingPrefabIcon);
                        }

                        while (AssetPreview.IsLoadingAssetPreviews())
                        {
                            if (prefabPoolOption.PrefabTransform)
                            {
                                prefabPreviewIcon = AssetPreview.GetAssetPreview(prefabPoolOption.PrefabTransform.gameObject);
                            }
                            else
                            {
                                prefabPreviewIcon = EZ_EditorUtility.LoadTexture(EZ_EditorAssets.MissingPrefabIcon);
                            }

                            System.Threading.Thread.Sleep(5);
                        }

                        EZ_EditorUtility.DrawTexture(prefabPreviewIcon, 100, 100);

                        EditorGUILayout.BeginVertical(GUILayout.MinHeight(prefabPreviewIcon.height));


                        if (!Application.isPlaying) //During Editor
                        {
                            prefabPoolOption.PrefabTransform = (Transform)EditorGUILayout.ObjectField("Prefab", prefabPoolOption.PrefabTransform, typeof(Transform), false);
                            prefabPoolOption.ShowDebugLog = EditorGUILayout.Toggle("Show Debug Log", prefabPoolOption.ShowDebugLog);
                            prefabPoolOption.InstancesToPreload = EditorGUILayout.IntSlider("Preload Qty", prefabPoolOption.InstancesToPreload, 0, 10000);


                            prefabPoolOption.PoolCanGrow = EditorGUILayout.Toggle("Allow Pool to grow", prefabPoolOption.PoolCanGrow);

                            if (prefabPoolOption.InstancesToPreload == 0 && !prefabPoolOption.PoolCanGrow)
                            {
                                EditorGUILayout.LabelField("*Preload Qty is 0 and Pool cannot grow. This pool will not be created.", EditorStyles.miniLabel);
                            }

                            if (prefabPoolOption.PoolCanGrow)
                            {
                                prefabPoolOption.EnableHardLimit = EditorGUILayout.Toggle("Enable Hard Limit", prefabPoolOption.EnableHardLimit);

                                if (prefabPoolOption.EnableHardLimit)
                                {
                                    prefabPoolOption.HardLimit = EditorGUILayout.IntSlider("Hard Limit", prefabPoolOption.HardLimit, 0, 10000);
                                }
                            }

                            prefabPoolOption.CullDespawned = EditorGUILayout.Toggle("Cull Despawned", prefabPoolOption.CullDespawned);

                            if (prefabPoolOption.CullDespawned)
                            {
                                prefabPoolOption.CullAbove = EditorGUILayout.IntSlider("Cull Above", prefabPoolOption.CullAbove, 0, 1000);
                                prefabPoolOption.CullDelay = EditorGUILayout.Slider("Cull Delay", prefabPoolOption.CullDelay, 0, 100);
                                prefabPoolOption.CullAmount = EditorGUILayout.IntSlider("Cull Amt", prefabPoolOption.CullAmount, 0, 100);
                            }

                            prefabPoolOption.recycle = EditorGUILayout.Toggle("Allow Pool to recycle", prefabPoolOption.recycle);
                        }
                        else //During Play mode
                        {
                            if (prefabPoolOption.PrefabTransform != null)
                            {
                                var itemInfo = EZ_PoolManager.GetPool(name);
                                if (itemInfo != null)
                                {
                                    var spawnedCount = itemInfo.spawnedList.Count;
                                    var despawnedCount = itemInfo.despawnedList.Count;
                                    EditorGUILayout.LabelField(string.Format("{0} / {1} Spawned", spawnedCount, despawnedCount + spawnedCount), EditorStyles.boldLabel);
                                    EditorGUILayout.Separator();
                                }
                            }

                            EditorGUILayout.LabelField("Preload Qty : " + prefabPoolOption.InstancesToPreload, EditorStyles.miniLabel);
                            EditorGUILayout.LabelField("Allow Pool to grow : " + prefabPoolOption.PoolCanGrow, EditorStyles.miniLabel);

                            if (prefabPoolOption.PoolCanGrow)
                            {
                                if (prefabPoolOption.EnableHardLimit)
                                {
                                    EditorGUILayout.LabelField("Hard Limit : " + prefabPoolOption.HardLimit, EditorStyles.miniLabel);
                                }
                            }

                            if (prefabPoolOption.CullDespawned)
                            {
                                EditorGUILayout.LabelField("Cull Above : " + prefabPoolOption.CullAbove, EditorStyles.miniLabel);
                                EditorGUILayout.LabelField("Cull Delay : " + prefabPoolOption.CullDelay, EditorStyles.miniLabel);
                                EditorGUILayout.LabelField("Cull Amt : " + prefabPoolOption.CullAmount, EditorStyles.miniLabel);
                            }

                            EditorGUILayout.LabelField("Allow Pool to recycle : " + prefabPoolOption.recycle, EditorStyles.miniLabel);

                        }
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.EndHorizontal();

                    }
                }

                EditorGUILayout.EndScrollView();
                EditorGUILayout.EndVertical();

                if (i_ToRemove != -1)
                {
                    poolManager.PrefabPoolOptions.RemoveAt(i_ToRemove);
                    isDirty = true;
                }
                if (i_ToInsertAt != -1)
                {
                    poolManager.PrefabPoolOptions.Insert(i_ToInsertAt, new EZ_PrefabPoolOption());
                    isDirty = true;
                }
                if (i_ToShiftUp != -1)
                {
                    var item = poolManager.PrefabPoolOptions[i_ToShiftUp];
                    poolManager.PrefabPoolOptions.Insert(i_ToShiftUp - 1, item);
                    poolManager.PrefabPoolOptions.RemoveAt(i_ToShiftUp + 1);
                    isDirty = true;
                }

                if (i_ToShiftDown != -1)
                {
                    var index = i_ToShiftDown + 1;
                    var item = poolManager.PrefabPoolOptions[index];
                    poolManager.PrefabPoolOptions.Insert(index - 1, item);
                    poolManager.PrefabPoolOptions.RemoveAt(index + 1);
                    isDirty = true;
                }
            }


            if (GUI.changed || isDirty)
            {
                EditorUtility.SetDirty(target);
            }

            this.Repaint();
        }
    }
}