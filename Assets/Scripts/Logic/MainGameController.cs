﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EZ_Pooling;

public class LinkObject
{
    public Transform PrefabObject;
    public int IdObject;

    public LinkObject(Transform prefabObject, int id)
    {
        this.PrefabObject = prefabObject;
        this.IdObject = id;
    }
}

public enum EObjectType
{
    UnKnown = -1,
    Bird = 0,
    Box = 1,
}

public class MainGameController : SingletonBehaviour<MainGameController> {

    public static Dictionary<EObjectType, LinkObject> DICT_MAP_OBJECT = new Dictionary<EObjectType, LinkObject>();

    [Header("Prefab")]
    public Transform PrefBird;
    public Transform PrefBox;
    public Transform PrefEffectSpawn;

	// Use this for initialization
	void Start () {
        DICT_MAP_OBJECT.Add(EObjectType.Bird, new LinkObject(PrefBird, 0));
        DICT_MAP_OBJECT.Add(EObjectType.Box, new LinkObject(PrefBird, 1));
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void HandleBtnConfirm_Clicked()
    {
        SObjectLinked objectLinked = ObjectManager.SharedInstance.CheckLinkObject();

        if (objectLinked.ObjectType == EObjectType.UnKnown)
        {
            Debug.Log("do not match");
        }
        else
        {
            Vector3 postionSpawnLinkedObject = Helper.FindMidPoint(objectLinked.ObjectA.TransLinkA.position, objectLinked.ObjectA.TransLinkB.position);
            EZ_Pooling.EZ_PoolManager.Spawn(DICT_MAP_OBJECT[EObjectType.Bird].PrefabObject, postionSpawnLinkedObject, Quaternion.Euler(0, 0, 0));
            // Spawn effect
            EZ_Pooling.EZ_PoolManager.Spawn(PrefEffectSpawn, postionSpawnLinkedObject, Quaternion.Euler(0, 0, 0));
        }
    }
}
