﻿using UnityEngine;
using System.Collections;

namespace Hd.Games.Shadow
{
    public class ObjectShadowController : MonoBehaviour {

        #region Declare Variables

        public Transform RTObject;

        private SpriteRenderer sprite, spriteFishObject;
        private Transform tf;

        #endregion Declare Variables

        private void Awake()
        {
            sprite = GetComponent<SpriteRenderer>();
            spriteFishObject = RTObject.GetComponent<SpriteRenderer>();
            gameObject.AddComponent<PolygonCollider2D>();

            tf = transform;

            sprite.color = new Color(0, 0, 0, GameSettings.SHADOW_ALPHA);
            sprite.sortingOrder = spriteFishObject.sortingOrder + GameSettings.SHADOW_SORT_LAYER;
            sprite.sprite = spriteFishObject.sprite;

        }

        void LateUpdate()
        {
           // sprite.sprite = spriteFishObject.sprite;
            tf.localScale = RTObject.localScale * GameSettings.FISH_SHADOW_SCALE;
            tf.position = RTObject.position + GameSettings.FISH_SHADOW_DISTANCE;
            tf.rotation = RTObject.rotation;

        }
    }
}