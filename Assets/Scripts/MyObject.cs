﻿using UnityEngine;
using System.Collections;

public class MyObject : MonoBehaviour {

    [Header("UI")]
    public Transform TfRoot;
    public Transform TransLinkA;
    public Transform TransLinkB;

    private Renderer RenderObject;
    private Vector3 distanceToMouse;


    void Awake()
    {
        RenderObject = GetComponent<Renderer>();
    }


    void OnMouseDown()
    {
        distanceToMouse = Camera.main.WorldToScreenPoint(TfRoot.position);
        distanceToMouse.x = Input.mousePosition.x - distanceToMouse.x;
        distanceToMouse.y = Input.mousePosition.y - distanceToMouse.y;
    }

    void OnMouseExit()
    {
       
    }

    void OnMouseDrag()
    {
        Vector3 curPos =
                  new Vector3(Input.mousePosition.x - distanceToMouse.x,
                  Input.mousePosition.y - distanceToMouse.y, distanceToMouse.z);

        Vector3 worldPos = Camera.main.ScreenToWorldPoint(curPos);
        TfRoot.position = worldPos;
    }
}
