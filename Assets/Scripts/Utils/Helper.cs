﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Helper
{
    /// <summary>
    /// Get angle between 2 vector
    /// </summary>
    /// <param name="vec1"></param>
    /// <param name="vec2"></param>
    /// <returns></returns>
    public static float Angle(Vector2 vec1, Vector2 vec2)
    {
        var dot = Vector3.Dot(vec1, vec2);
        // Divide the dot by the product of the magnitudes of the vectors
        dot = dot / (vec1.magnitude * vec2.magnitude);
        //Get the arc cosin of the angle, you now have your angle in radians 
        var acos = Mathf.Acos(dot);
        //Multiply by 180/Mathf.PI to convert to degrees
        var angle = acos * 180 / Mathf.PI;

        return angle;
    }

    /// <summary>
    /// Get Angle of 3 point (at point 2)
    /// </summary>
    /// <param name="point1"></param>
    /// <param name="point2"></param>
    /// <param name="point3"></param>
    /// <returns></returns>
    public static float Angle(Vector2 point1, Vector2 point2, Vector2 point3)
    {
        Vector2 vec1 = (point2 - point1).normalized;
        Vector2 vec2 = (point2 - point3).normalized;

        return Vector2.Angle(vec1, vec2);
    }

    /// <summary>
    /// Get Angle of 3 point (at point 2)
    /// </summary>
    /// <param name="point1"></param>
    /// <param name="point2"></param>
    /// <param name="point3"></param>
    /// <returns></returns>
    public static float Angle(Vector3 point1, Vector3 point2, Vector3 point3)
    {
        Vector2 vec1 = (point2 - point1).normalized;
        Vector2 vec2 = (point2 - point3).normalized;

        return Vector2.Angle(vec1, vec2);
    }


    /// <summary>
    /// Find point on line segment defined by vectors A and B
    /// </summary>
    /// <param name="pointA"></param>
    /// <param name="pointB"></param>
    /// <param name="percent"> percent distance A->B</param>
    /// <returns></returns>
    public static Vector3 LerpDistance(Vector3 pointA, Vector3 pointB, float percent)
    {
        Vector3 newPoint = percent * Vector3.Normalize(pointB - pointA) + pointA;
        return newPoint;
    }

    /// <summary>
    /// Find point on line segment defined by vectors A and B
    /// </summary>
    /// <param name="pointA"></param>
    /// <param name="pointB"></param>
    /// <param name="percent"> percent distance A->B</param>
    /// <returns></returns>
    public static Vector3 LerpOverDistance(Vector3 pointA, Vector3 pointB, float percent)
    {
        Vector3 newPoint = percent * Vector3.Normalize(pointB - pointA) + pointB;
        return newPoint;
    }

    /// <summary>
    /// Find new point by centerPoint + angle at centerPoint
    /// </summary>
    /// <param name="centerPoint"></param>
    /// <param name="angle"></param>
    /// <param name="distance"> distance from centerpoint to newPoint</param>
    /// <returns></returns>
    public static Vector3 LerpDistanceByAngle(Vector3 centerPoint, float angle, float distance)
    {
        Vector3 newPoint = centerPoint;

        float radianAngle = Mathf.Deg2Rad * angle;

        newPoint.x += Mathf.Cos(radianAngle) * distance;
        newPoint.y += Mathf.Sin(radianAngle) * distance;

        return newPoint;
    }

    /// <summary>
    /// Find new point by centerPoint + angle at centerPoint
    /// </summary>
    /// <param name="centerPoint"></param>
    /// <param name="angle"></param>
    /// <param name="distance"> distance from centerpoint to newPoint</param>
    /// <returns></returns>
    public static Vector3 LerpDistanceByAngle(Vector3 centerPoint, float angle, float distanceX, float distanceY)
    {
        Vector3 newPoint = centerPoint;

        float radianAngle = Mathf.Deg2Rad * angle;

        newPoint.x += Mathf.Cos(radianAngle) * distanceX;
        newPoint.y += Mathf.Sin(radianAngle) * distanceY;

        return newPoint;
    }

    public static Vector3 LerpDistanceByDirect(Vector3 currentPoint, Vector3 direction, float percent)
    {
        return direction * percent + currentPoint;
    }

    public static Vector3 FindMidPoint(Vector3 vectorA, Vector3 vectorB)
    {
        return (vectorA + vectorB) / 2;
    }
}

