﻿using UnityEngine;

public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T m_Instance;
	public static T SharedInstance
    {
        get
        {
			return m_Instance;
        }
    }       

    protected virtual void Awake()
    {
		if (m_Instance == null)
        {
			m_Instance = this as T;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.Log("Kill Object " + gameObject.name);
            DestroyImmediate(gameObject);
            return;
        }
    }
}

