﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hd.Games.Shadow
{
    public static class GameSettings
    {
        #region Shadow Config

        public static Vector3 FISH_SHADOW_DISTANCE = new Vector3(-3f, 3f, 0);
        public static float FISH_SHADOW_SCALE = 0.85f;
        public const float SHADOW_ALPHA = 0.5f;
        public const int SHADOW_SORT_LAYER = -1;

        #endregion Shadow Config

        #region Tag Name

        #endregion

        #region Default value
        public static float DEFAULT_CHECK_LINK_OFFSET = 0.1f;
        #endregion



    }
}   