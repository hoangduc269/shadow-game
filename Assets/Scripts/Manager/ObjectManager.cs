﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[System.Serializable]
public struct SObjectLinked
{
    public EObjectType ObjectType;
    public MyObject ObjectA;
    public MyObject ObjectB;
    public bool IsLinked;
}


public class ObjectManager : SingletonBehaviour<ObjectManager> {

    public SObjectLinked[] ListObjectLinked;

   // Use this for initialization
    void Start () {
	
	}

    public SObjectLinked CheckLinkObject()
    {
        SObjectLinked linkedResultObject = new SObjectLinked();
        linkedResultObject.ObjectType = EObjectType.UnKnown;

        foreach (var linkObject in ListObjectLinked)
        {
            if (!linkObject.IsLinked)
            {
                // check link object 
                if (Vector2.Distance(linkObject.ObjectA.TransLinkA.position, linkObject.ObjectB.TransLinkA.position) < 0.1f
                        && Vector2.Distance(linkObject.ObjectA.TransLinkB.position, linkObject.ObjectB.TransLinkB.position) < 0.1f)
                {
                    linkedResultObject = linkObject;
                }

            }
        }
        return linkedResultObject;
    }


}
